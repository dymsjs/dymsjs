"use strict";

require('dotenv').config();

//Prototype for CamelCase classes
String.prototype.toCamelCase = function() {
    return this
    	.toLowerCase()
        .replace(/\s(.)/g, function($1) { return $1.toUpperCase(); })
        .replace(/\s/g, '')
        .replace(/^(.)/, function($1) { return $1.toUpperCase(); });
};

//Set framework as app global variable
global.app = this;

/**
 * Module loader helper
 * @param file
 * @param type
 * @returns {*}
 */
//loaded modules
var modules = {};

var m = function(file, type) {
    if (typeof(type) === "undefined") {
        type = "app";
    }

    var module_file;

    switch (type) {
        case "node":
            module_file = file;
            break;
        case "system":
            module_file = ((typeof(config) === "undefined") ? "./" : config.paths.modules.system) + file + ".js";
            break;
        case "app":
        default:
            module_file = ((typeof(config) === "undefined") ? "../" : config.paths.modules.app) + file + ".js";
            break;
    }

    var return_module;

    //if module is loaded before, return the loaded before module
    if (typeof(modules[file]) !== "undefined") {
        return modules[file];
    }

    //check errors loading modules
    try {
        return_module = require(module_file);
        modules[file] = return_module;
    } catch (e) {
        console.log("ERROR", e);
    }

    return return_module;
};

//Load config and merge with defaults
var _ = m("underscore","node");

var config = m("config_defaults", "system");

global.config = app.config = _.extend(
    config,
    m("config")
);

var module_exports = {};

//Assign module exports
module_exports.m = module_exports.module = m;
module_exports.require = module_exports.r = function(file) {
    return m(file, "node");
};
module_exports.library = module_exports.l= function(file){
    return m(file, "system");
};


//Autoload modules system and app
for (var mod in config.autoload.system) {
    module_exports[ config.autoload.system[mod] ] = m( config.autoload.system[mod] , "system");
}
for (var modapp in config.autoload.app) {
    module_exports[ config.autoload.app[modapp] ] = m( config.autoload.app[modapp] );
}


//Ensure store is loaded
module_exports.store = m("store", "system");

//Works with variables using store
var v = function(name_of_variable, value_of_variable) {
    if (typeof(value_of_variable) != "undefined") {
        //Save variable
        module_exports.store.set(name_of_variable, value_of_variable);

        return value_of_variable;
    } else {
        return module_exports.store.get(name_of_variable);
    }
};
module_exports.v = module_exports.variable = v;

//Set default values in variables
for (var def_var in config.defaults) {
    v(def_var, config.defaults[def_var]);
}

//Set globals
for (var g in config.global) {
    if (typeof(module_exports[ config.global[g] ]) === "undefined") {
        module_exports["log"]("You can't load as global if it isn't autoloaded: "+config.global[g]);
    } else {
        global[config.global[g]] = module_exports[config.global[g]];
    }
}

//Assign libraries
for (var expmod in module_exports){
    app[expmod] = module_exports[expmod];
}