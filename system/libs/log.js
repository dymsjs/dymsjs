"use strict";

/**
 * Better log of object
 * @return void
 */

var levels = {
    error: 0, 
    warn: 1, 
    info: 2, 
    verbose: 3, 
    debug: 4, 
    silly: 5 
};

var initialized = false;
var winston,
    moment,
    level,
    channel;

/**
 * Configure options for winston libraries and its transports
 * @return {boolean} false if is error
 */
var setup = function () {
    if (!initialized) {
        moment = app.r("moment");

        if (app.config.log.winston) {
            winston = app.r("winston");
            if (app.config.log.fluent) {
                var f = app.r("winston-fluent").Fluent;
            }
        } else {
            return false;
        }

        if (app.config.log.file) {
            winston.add(
                winston.transports.File, {
                    filename: app.config.paths.logs + app.config.log.filename
                }
            );
        }

        //Use a best format for Console print
        winston.remove(winston.transports.Console);
        winston.add(winston.transports.Console, {
            timestamp: function () {
                return moment().format();
            },
            colorize: true,
            formatter: function (options) {
                // Return string will be passed to logger.
                return options.timestamp() + ' ' + channel + ' ' + options.level.toUpperCase() + ' ' + (options.message ? options.message : '') +
                    (options.meta && Object.keys(options.meta).length && options.meta.context.empty != "" ? '\n\t' + JSON.stringify(options.meta.context) : '');
            }
        });
    }
    initialized = true;
}

/**
 * Configure the Fluent options because the tag and label is setting up
 * with each log call
 * @param {string} channel  the channel id
 * @param {string} level  log level
 * @return {void}
 */
var setup_fluent = function (channel, level) {
    //Fluent transport is setup in each log call
    //Log calling log("channel.level", message, context object)
    try{
        winston.remove(winston.transports.Fluent);
    }catch(e){}
    
    winston.add(
        winston.transports.Fluent, {
            tag: app.config.log.fluentTag,
            label: arguments[0].replace("." + level, ""), //remove level from argument
            host: app.config.log.fluentHost || 'localhost',
            port: app.config.log.fluentPort || 24224
        });
}

module.exports = function () {
    setup(); //Exec initial configuration

    var _ = app.r("underscore");

    var args = _.values(arguments);

    //Only write logs if it is enabled
    if (app.config.log.enabled) {
        //Get level from first argument
        level = args[0].toString().match(/([^\.]+)$/g)[0];
        channel = args[0].toString().replace("." + level, "");

        //Set default level
        if (typeof levels[level] === "undefined") {
            level = app.config.log.level;
        }

        //Check the error level sets in config and ignore all errors over the level
        if (levels[level] > levels[app.config.log.level]) {
            return false;
        }

        if (app.config.log.winston) {
            //Log calling log("level", message, context object)
            if (app.config.log.fluent) {
                setup_fluent(channel, level);
            }

            winston.log(
                //log level
                level,
                //message
                (args[1] && args[1].toString()) || args[0].toString(),
                //context extra data
                (typeof args[2] === "object") ? {
                    context: args[2]
                } : {
                    context: {
                        'empty': ''
                    }
                }
            );

        } else {
            //Matching winston levels with console levels
            var console_matching_log = {
                emerg: "error",
                alert: "error",
                crit: "error",
                error: "error",
                warning: "warn",
                notice: "warn",
                info: "info",
                debug: "log"
            };

            //Add timestamp to log
            args.unshift("[" + moment().format() + "]");

            //Call to console with all arguments
            console[console_matching_log[level]].apply(null, args);
        }
    }
};