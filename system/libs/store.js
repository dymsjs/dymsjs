"use strict";

var Stapes = require("stapes");

var Redux = Stapes.subclass({
    /**
	 * Returns all variables object
     * @returns {*}
     */
	getState: function(){
		return this.getAll();
	}
});

module.exports = new Redux();