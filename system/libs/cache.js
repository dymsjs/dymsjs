"use strict";

let cacheman = app.r('cacheman-redis-promise');

class Cache {

    constructor() {
        this._connected = false;
    }

    _connect() {
        this._cache = cacheman({
            host: config.redis.host,
            port: config.redis.port
        });
    }

    /**
     * Save in cache
     * @param key
     * @param value
     * @returns Promise
     */
    set (key, value) {
        if (!this._connected){
            this._connect();
            this._connected = true;
        }
        return this._cache.set(key, value);
    }

    /**
     * Get the key and if doesn't exists then returns and save default_value
     * @param key
     * @param default_value
     * @returns {*}
     */
    get (key, default_value){
        if (!this._connected){
            this._connect();
            this._connected = true;
        }
        return this._cache.fetch(key, default_value);
    }

}

module.exports = new Cache();