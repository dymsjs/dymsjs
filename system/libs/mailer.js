"use strict";

var Stapes = app.r("stapes");
var nodemailer = app.r("nodemailer");

var Mailer = Stapes.subclass({
    constructor: function(){
        //Generate an mailing event buffer
        this.on('send', function(opt){
            this.dispatch(opt);
        });
    },

    /**
     * Function for generating a buffer of event calls processing in a queue
     * @param opt The mail options
     */
	send: function(opt){
        //Send mail to event queue
        this.emit('send',opt);
	},
    /**
     * Send the email
     * @param opt The mail options
     */
    dispatch: function(opt){

        // create reusable transporter object using the default SMTP transport
        var transporter = nodemailer.createTransport({
            host: config.mailer.host,
            port: config.mailer.port,
            secure: config.mailer.secure, // secure:true for port 465, secure:false for port 587
            auth: {
                user: config.mailer.auth.user,
                pass: config.mailer.auth.pass
            }
        });

        // setup email data with unicode symbols
        var mailOptions = {
            from: ( (opt.from)?opt.from : config.mailer.from ), // sender address
            to: opt.to, // list of receivers
            subject: opt.subject, // Subject line
            text: opt.text, // plain text body
            html: ( (opt.html)?opt.html : opt.text ) // html body
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
                return log('mailer.error', error);
            }
            log('mailer.info','Mail sent!', {
                from: info.envelope.from,
                to: info.envelope.to,
                messageId: info.messageId,
                response: info.response
            } );
        });
    }
});

module.exports = new Mailer();