"use strict";

//Basic class
var Stapes = require("stapes");

app.Command = Stapes.subclass({
    /**
     * Method for customize the command line usage
     * @returns {*}
     */
    initParams : function(){
        return app.r('optimist')
            .usage( this.usage );
    },
    //returns the cli arguments
    params : function(){
        return this.initParams().argv;
    },
    //default run method
    run : function(){
        var args = this.initParams();
    },
    //Default command info
    info : function(){
        return 'Sample command file';
    },
    //Default print arguments help for run method
    help: function(){
        var args = this.initParams();
        args.showHelp();
    }
});

// ============================================
//    COMMANDS LOAD

//pads right
String.prototype.rpad = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = str + padString;
    return str;
};

//Load all commands
var fs = require('fs');
var files = fs.readdirSync( config.paths.commands );
var commands = [];
for (var i in files){
    var command_name = files[i].replace('.js', '');
    commands[command_name] = require( config.paths.commands + files[i] );
    commands[command_name].name = command_name;
    commands[command_name].usage = "Usage:$0 " + command_name;
};

//Preparing default params
var argv = app.r('optimist')
    .describe('help','Display this help')
    .describe('time','Display execution time at finish')
    .boolean(['help','time'])
    .argv;

console.log(config.commandline.name + ' ' + config.commandline.version + '\n');

if (argv.time){
    var timeStart = new Date();
}
//check if command exits
if (typeof(commands[ argv._[0] ]) !== 'undefined'){
    if (argv.help){
        commands[argv._[0]].help();
    }else{

        if (typeof(argv._[1]) !== 'undefined' && typeof(commands[argv._[0]][argv._[1]]) !== 'undefined'){
            //If method exists in command then call it
            commands[argv._[0]][argv._[1]]();
        }else{
            //Calling default method
            commands[argv._[0]].run();
        }
    }
}else{
    //No valid command so print all commands info
    console.log('Command not found!');
    console.log('\nAvailable commands:');

    for (var i in commands){
        console.log ('  '+ i.rpad(' ', 20) + '\t' + commands[i].info());
    }
    console.log('\n<command> --help \t Display help of command');
    console.log('<command> --time \t Display execution time');
}

process.on('exit', function () {
    console.log('\nFinished command\n');
    if (argv.time){
        var Duration = app.r('duration');
        var duration = new Duration(timeStart, new Date());
        console.log('Execution time: '+duration.toString(1));
    }
});