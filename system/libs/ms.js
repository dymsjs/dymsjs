'use strict';

var Stapes = app.r('stapes');
var connected = false;

var Obj = Stapes.subclass({
    /**
     * Pass the options to the transport plugin and initialize
     * @param {Object} options The params for Seneca instance and transport plugin
     * @returns {Object} An microservice instance
     */
    constructor: function(options){
        var self = this;

        self.senecaInstance = app.r('seneca')({
            // Set option via Seneca constructor.
            debug: {
                undead: true
            }
        });

        if (options.listen){
            self.senecaInstance.listen(options.listen);
        }

        if (options.client){
            self.senecaInstance.client(options.client);
        }
        
        // Check plugins ready callback
        self.senecaInstance.ready(function(){
            connected = true;
            self.emit(self.CONNECTED);
        });

        return self;
    },
    /**
     * Add a microservice function handler
     * @param {Object} obj The object to subscribe handler
     * @param {Function} cb The function handler (msg, callback)
     * @returns {Object} The microservice instance
     */
    add: function (obj, cb) {
        //If you call without connected, then wait to connect and then add method
        if (!connected) {
            this.on(this.CONNECTED, function () {
                this.add(obj, cb);
            });
        } else {
            this.senecaInstance.add(obj, cb);
        }
        return this;
    },
    /**
     * Call to microservice function handler
     * @param {Object} obj The object with params and topic
     * @param {Function} cb Callback Function (error, result)
     * @returns {Object} The microservice instance
     */
    call: function (obj, cb) {
        //If you call without connected, then wait to connect and call method
        if (!connected) {
            this.on(this.CONNECTED, function () {
                this.call(obj, cb);
            });
        } else {
            this.senecaInstance.act(obj, cb);
        }
        return this;
    },
    log: function(log_data) {
        this.senecaInstance.log(log_data);
    },
    //CONSTANTS
    CONNECTED: 'connected'
});

module.exports = function(options){ 
    return new Obj(options); 
};

