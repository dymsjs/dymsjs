"use strict";
/*
This file is the core default framework configuration.
If you want to overwrite this configuration, please use your app/config.js
 */
module.exports = {
	commandline:{
		name: "Command Line Tools",
		version: "1.0.0"
	},
	log: {
		level: "debug",
		enabled: true,
		winston: false,
		fluent: false,
		fluentTag: "nodejs",
		fluentHost: 'localhost',
		fluentPort: 24224,
		file: false,
		filename: "file.log"
	},
	autoload:{
		//Load system modules
		system: [
			"log",
			"store"
		],
		//App modules empty by default
		app: [] 
	},
	//Load these modules as global object
	global: [
		"log"
	],
	//configure relative paths from framework.js file
	paths: {
		modules:{
			system: __dirname + "/libs/",
			app: __dirname +  "/../libs/",
		},
        commands: __dirname + "/../commands/",
		views: __dirname + "/../views",
		logs: __dirname + "/../logs/"
    },
	//Default variables
	defaults: {

	}
};