"use strict";

var http_transport_options = {
	type: 'http',
	port: '8000',
	host: 'localhost',
	protocol: 'http'
};

var redis_transport_options = {
	type: 'redis',
	port: '6379',
	host: 'localhost'
};


var Command = app.Command.subclass({
	name: "ms-test",
	info: function(){
		return "Test of microservices";
	},
	run: function(){
		var ms = app.l("ms_new")();

		//Add a new add method
		ms.add({topic: "math", method: "add" }, function(msg, reply){
			reply(null, {answer: msg.a + msg.b});
			log('mstest.info', 'Message before callback');
		}).call({topic: "math", method: "add", a:2 , b:6}, function(error, response){
			if(!error){
				log("test.info", response);
			}else{
				log(error);
			}
		});
	},
	http_service: function(){
		var ms = app.l("ms_new")({
			listen: http_transport_options
		});

		//Add a new add method
		ms.add({ topic: "math", method: "add" }, function (msg, reply) {
			reply(null, { answer: msg.a + msg.b });
			log('mstest.info', 'Message before callback');
		});
	},
	http_request_service: function(){
		var ms = app.l("ms_new")({
			client: http_transport_options
		});

		//Call the method
		ms.call({ topic: "math", method: "add", a: 2, b: 6 }, function (error, response) {
			if (!error) {
				log("test.info", response);
			} else {
				log(error);
			}
		});
	},
	redis_service: function() {
		var ms = app.l("ms_new")({
			listen: http_transport_options
		});

		//Add a new add method
		ms.add({ topic: "math", method: "add" }, function (msg, reply) {
			reply(null, { answer: msg.a + msg.b });
			log('mstest.info', 'Message before callback');
		});
	},
	redis_service_request: function() {
		var ms = app.l("ms_new")({
			client: http_transport_options
		});

		//Call the method
		ms.call({ topic: "math", method: "add", a: 2, b: 6 }, function (error, response) {
			if (!error) {
				log("test.info", response);
			} else {
				log(error);
			}
		});
	}
});

module.exports = new Command();