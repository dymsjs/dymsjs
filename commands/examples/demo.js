"use strict";

let Command = app.Command.subclass({
	initParams: function () {
		return app.r('optimist')
			.usage(this.usage)
			.describe('param1', 'Este sería el primer parámetro')
			.describe('s', 'Flag con parámetro corto')
			.boolean('s')
			.default('param1', '5');
	},
	run: function(){
		var args = this.params();

		var param1 = args.param1 || "default"; //parámetro pasado en la línea de comandos
		if (args.s){
			//parámetro como flag
		}

		log("demo.info", "Log demo with winston");
		log("demo.error", "Wow this is an error with extra params!!", {error:true});
	}

});

module.exports = new Command();