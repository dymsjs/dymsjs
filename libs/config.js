"use strict";

module.exports = {
    commandline:{
        name: "DyMS.js",
        version: "1.0.0"
    },
	log: {
        level: process.env.LOGLEVEL || "info",
        enabled: true,
        winston: true,
        fluent: false,
        fluentTag: "nodejs",
        file: false,
        filename: "dymsjs.log"
    },
    autoload:{
        //Load system modules
        system: [
            "log",
            "store"
        ],
        //App modules empty by default
        app: [
            
        ]
    },
	global: ["log", "store"],
    ms: {
        mqtt: {
            
        },
        http: {

        },
        redis: {

        }
    },

    //Default variables accesible by app.v("name")
    defaults:{
        "mivariable": 5
		// variables
	}
};