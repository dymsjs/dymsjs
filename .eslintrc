{
  "parserOptions": {
    "ecmaVersion": 2017
  },
  "env": {
    "node": true,
    "jest": true,
    "es6": true,
    "mongo": true
  },
  "globals": {
    "log": true,
    "app": true
  },
  "rules": {
    "semi": [
      2,
      "always"
    ],
    // Possible Errors
    "no-console": 1,
    "valid-jsdoc": 2, // We want to generate good documentation, see JSDoc API for more info

    // Best Practices
    "consistent-return": 0,
    "default-case": 2, // We want to have a default case always, and it can't be empty
    "eqeqeq": 0,
    "no-eq-null": 2,
    "no-floating-decimal": 2,
    "no-script-url": 0,
    "no-self-compare": 2,

    // Stylistic Issues
    "camelcase": 0,
    "comma-style": [2, "last"],
    "no-lonely-if": 2,
    "no-multiple-empty-lines": [2, {"max": 2}],
    "no-nested-ternary": 2,
    "spaced-line-comment": [0, "always", {"exceptions":["-","+"]}],
    "quotes": 0,
    "max-statements":[2, 30],
    "max-depth":[1, 4],
    "complexity":[2, 9]
  },
  "extends": "eslint:recommended"
}