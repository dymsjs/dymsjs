dymsjs · DIGIO y Microservicios para NodeJS
===============

Este microframework tiene como objetivo facilitar la creación de proyectos en NodeJS.

dymsjs dispone de las siguientes características:

* Creación de comandos con soporte de parámetros, medición de tiempo de ejecución
* Storage unificado a través de librería
* Sistema de log integrado y configurable
* Integración con servidores PubSub para implementación de comunicación de Microservicios
* Fácil configuración a través de variables de entorno, ficheros .env y ficheros de configuración
* Integración con librería de envío de mails
* Integración con caché en Redis

## Cómo empezar

Nada más clonar el repositorio el repositorio necesitarás instalar las dependencias.

```bash
npm install
```

Ahora podrás ejecutar el cli y visualizar los comandos disponibles.

```
nodejs run
```

O bien si estás en linux lanzarlo como script de shell

```
./run
```

## Parámetros por defecto

Tienes dos parámetros que podrás usar siempre, para visualizar la ayuda del comando y otro para ver el tiempo que ha estado ejecutando.

```bash
./run --help   #muestra los comandos
./run --time   #muestra el tiempo de ejecución
```

## Commands

Los **commands** son el principal punto para añadir nueva funcionalidad. Permiten añadir nuevos puntos de entrada para lanzar ejecuciones de distintos procesos.

Están integrados con un intérprete de parámetros para poder trabajar con cualquier argumento que se pase de forma estándar.

Esta es la estructura básica de un comando `commands/basic.js` que permitirá ejecutar un comando  `basic`

```javascript
"use strict";

let Command = app.Command.subclass({
    info: function(){
        return "Comando básico"
    },
	run: function(){
		var args = this.params();
		log("basic.info", "Este es un comando básico");
	}
});

module.exports = new Command();
```

Podemos ahora ejecutar 
```
./run
``` 

y ver que el nuevo comando está disponible en el listado, con el texto que hemos indicado en el método `info`.

Pero lo normal es que a los comandos podamos indicar otros parámetros que necesitemos especificar en la línea de comandos, esto lo haremos especificando el método `initParams` en el comando.

Este sería un ejemplo definiendo la ayuda de los parámetros.

```javascript
let Command = app.Command.subclass({
    info: function(){
        return "Comando básico"
    },
    initParams: function () {
		return app.r('optimist')
			.usage(this.usage)
			.describe('param1', 'Este sería el primer parámetro')
			.describe('s', 'Flag con parámetro corto')
			.boolean('s')
			.default('param1', '5');
	},
	run: function(){
		var args = this.params();
		log("basic.info", "Este es un comando básico");
	}
});

module.exports = new Command();
```

Para ver todas las opciones de cómo especificar los parámetros de la línea de comandos ver más info en la documentación de la librería [optimist](https://www.npmjs.com/package/optimist).

## Objeto global del framework

Todos los métodos y librerías del framework tienen disponible el objeto `app`, este objeto permite cargar otras librerías, y acceder a todas ellas a modo de singleton.

## Variables de estado

El framework implementa un contenedor de variables de estado, de forma similar a lo que hacía **Redux** y **Vuex** en sus comienzos.

Se puede guardar una variable de estado con y obtenerla utilizando el método app.v

```javascript
app.v('nombre_variable', valor);  //Guarda una variable de estado
app.v('nombre_variable');         //Obtiene el valor de una variable de estado

app.store.getState();             //Obtiene todas las variables de estado almacenadas
```

### ¿Porqué es útil las variables de estado?

Permite guardar variables que son accesibles desde cualquier parte del código (a través de cualquier librería) sin tener que generar variables globales.

Es fácil de debuggear el estado de todo un flujo simplemente viendo todas las variables de estado con el método  `getState`

### ¿Dónde no debo usar las variables de estado?

No lo uses para almacenar cualquier dato que tengas que reescribir en un bucle o sucesivas llamadas sobre todo, si tu comando queda permanentemente en ejecución.

Recuerda que las variables locales tienen una vida corta, mientras el método es llamado, y se elimina una vez terminada la llamada de un método, el colector de basura de NodeJS `GC` te prevendrá para liberar memoria cuando sea necesario.

Si usamos variables de estado para cosas que no son estados, irás consumiendo cada vez más y más memoria, y podría ser un problema.

## Carga de librerías

El framework posee varios atajos para cargar librerías:

```javascript
// Carga una librería global, es equivalente a un require('nombre') pero no genera una excepción, simplemente avisa y produce una degradación
app.r('nombre');     

// Carga una librería del core del framework, por ejemplo la librería para comunicar microservicios
app.l('ms');

// Carga una librería de aplicación, aquellas que se encuentran en la carpeta libs
app.m('milibreria');
```

Para las librerías de aplicación se buscarán en la carpeta libs, aunque siempre podremos configurar una diferente desde la configuración.

Se recomienda usar estos metodos en lugar de require, salvo que se quiera utilizar los nuevos métodos `import` de Node 9.

El método devuelve también la instancia de la librería por lo que se puede usar desde el singleton `app` y desde la instancia.

```javascript
var milibreria = app.m('milibreria');

app.milibreria.method;		// ok
milibrería.method;		//ok
```

## Convención para logear

El framework lleva integrada la librería `winston` que permite extender los logs a varias alternativas, en concreto el framework está preparado para utilizar, la salida estándar, fichero y un socket JSON.

El tipo de salida del log se configurará desde los ficheros de configuración del framework, se puede ver la ayuda más adelante.

Desde la aplicación siempre podremos logear usando `app.log` o simplemente `log`:

```javascript
log("accion.info", "Esto sería un log de nivel info para una acción");
log("accion2.debug", "Log de debug");
log("accion3.error", "Indicar un error para la acción 3", { extra: 5});
log("accion4.warning", "Indicar una advertencia para la acción 4");
```

La convención es que el primer parámetro se indique un identificador de acción (alfanumérico sin espacio) seguido de un punto `.` y del nivel de log.

Un tercer parámetro como objeto, se le puede pasar con información de contexto.

Es importante mantener esta convención porque será mucho más fácil leer los logs y disponer de información contextualizada.

## Configuración del framework

Existen dos ficheros que permiten configurar el framework en muchos aspectos, el fichero principal lo podemos encontrar en `system/config_defaults.js` es el fichero que dispone de todos los valores por defecto, no deberemos editar este fichero nunca, salvo que tengamos claro que queremos añadir nueva funcionalidad en el core.

A la hora de configurar una aplicación, tendremos que sobreescribir los valores de configuración en el fichero `libs/config.js`.

Todas las rutas por defecto donde podemos encontrar los comandos, librerías, módulos, y vistas se pueden personalizar desde la configuración, donde están definidas las rutas por defecto.

## Uso de vistas

El framework tiene integrado por defecto `nunjucks`, un motor de vistas inspirado en **jinja2** conocido en python o **twig** de PHP.

Por defecto se espera que las vistas estén en el path definido en `app.config.paths.views`.

[Ver documentación de nunjucks](https://mozilla.github.io/nunjucks/)

## Estructura de módulos

Llamaremos **módulos** a todas las librerías que podemos encontrar en la carpeta libs y que cargaremos con `app.m("librería")`.

Se han  creado una serie de carpetas para ordenar el código con el siguiente objetivo:

* `models`. Definen librerías para acceder a objetos de datos, actualizar, crear, etc... sirven para modelizar un tipo de entidad que queramos representar.

* `repositories`. Se trata de un conjunto de objetos de tipo `model`. Pueden estar conectados a una base de datos, sistema de cache, o bien a un servicio REST o similar.

* `use-cases`. Representan rutinas que modelizan un proceso completo, por ejemplo, **obtener_listado_usuarios_con_correo_gmail**. Sirven para centralizar la ejecución de procesos definidos en nuestra lógica.

* `utils`. Son librerías de propósito general que ayudarán en nuestra aplicación.

## Librerías del core

Existen algunas librerías inicales integradas en el core del framework. Estas librerías podremos cargarlas con `app.l("librería")` y se encuentran dentro de la carpeta **system/libs**.

### Cache

Implementa una forma simple de permanecer objetos en un servidor Redis, utilizando `cacheman`.

Implementa dos métodos simples `get` y `set` para obtener un valor de la cache y para guardar un valor en la caché, respectivamente.

```javascript
app.l('cache');  // Cargar la librería

//guardar un valor
app.cache.set('identificador', valor);

//obtener un valor
app.cache.get('identificador');
```

Los datos de conexión a Redis se guardan en el fichero de configuración.

### Mailer

Esta librería permite generar una cola de envíos que son resueltos por la librería npm **nodemailer**.

Ejemplo:

```javascript
var mailer = app.l('mailer');

mailer.send({
	to: msg.to,
	subject: msg.subject,
	text: msg.text,
	html: ( (msg.html)?msg.html : msg.text )
});
```

### Microservice transport

Esta librería permite comunicar microservicios a través de un servidor PubSub, o bien a través de HTTP.

Se usa por detrás la librería `seneca.js`, por lo que cualquier extensión de transport es utilizable en esta librería.

Actualmente hay disponibles los siguientes transports:

* NATS
* MQTT
* NSQ
* AMPQ
* STOMP
* RabbitMQ
* Redis
* BeanStalk
* Kafka
* ActiveMQ
* HTTP

La librería permite añadir funciones para ejecutar de forma remota `add`, estas funciones son accesibles para todos aquellos microservicios conectados. Para llamar a esta función usaremos el método `call` para hacer las llamadas a procedimientos remotos.

Aquí un ejemplo usando el transport HTTP que viene por defecto:

```JavaScript
var http_transport_options = {
	type: 'http',
	port: '8000',
	host: 'localhost',
	protocol: 'http'
};

var ms = app.l("ms")({
	listen: http_transport_options
});

//Add a new add method
ms.add({ topic: "math", method: "add" }, function (msg, reply) {
	reply(null, { answer: msg.a + msg.b });
	log('mstest.info', 'Message before callback');
});
```

Desde otro proceso podremos utlizar la conexión para conectarnos y llamar al método.

```JavaScript
var ms = app.l("ms")({
	client: http_transport_options
});

//Call the method
ms.call({ topic: "math", method: "add", a: 2, b: 6 }, function (error, response) {
	if (!error) {
		log("test.info", response);
	} else {
		log(error);
	}
});
```

Una call a un método espera siempre a la respuesta, si no quieres esperar una respuesta o no es importante, se puede enviar una respuesta de `ACK` para seguir procesando el método sin aportar respuesta posterior.

